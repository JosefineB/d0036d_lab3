import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;

public class GameServer {
	
	private int tcpPort;
	private int udpPort;
	private int multicastPort;
	private static ArrayList<ClientThread> clients = new ArrayList<ClientThread>();
	PrintWriter out; //TCP
	BufferedReader input; //TCP
	private int playerNr;
	
	
	public GameServer(int tcpPort, int udpPort, int multiPort){
		this.tcpPort = tcpPort;
		this.udpPort = udpPort;
		this.multicastPort = multiPort;
	}
	
	public void initServer(){
		//Create a thread for each client, instead of handling it in the main thread
		// the connection gets passed to a new thread.
		try{
			ServerSocket serverSocket = new ServerSocket(tcpPort, 0, InetAddress.getByName("localhost")); 
			//When not using localhost an ip address to use needs to be specified like below.
			//ServerSocket serverSocket = new ServerSocket(tcpPort, 0, InetAddress.getLocalHost());
			DatagramSocket udpSocket = new DatagramSocket(udpPort);
			MulticastSocket multiSocket = new MulticastSocket(multicastPort); 
			InetAddress ia = InetAddress.getByName("239.255.255.250");
			multiSocket.joinGroup(ia); //it's in this socket the server listens for clients and sends out updates of gamestate
			ClientListener requests = new ClientListener(multiSocket, "LocalServer", serverSocket);
			requests.start();
			while(true){ //Standard for most java servers
				System.out.println("Waiting for clients..."); // 1.Wait for client request
				Socket clientSocket = serverSocket.accept(); // 2. Accept client.
				System.out.println("Client is connected."); // 3. Repeat process.
				try{
					this.input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					this.out = new PrintWriter(clientSocket.getOutputStream(), true);
				}catch(IOException e){
					System.out.println("Couldn't open stream" + e.toString());
				}
				playerNr++;
				//What we previously did inside the server loop is now moved to the run method in the client thread,
				//that makes it possible to handle multiple inputs at once, meaning not having to go in an sequantial order 
				// of connecting client, taking input, closing client.
				ClientThread clientThread = new ClientThread(clientSocket, input, out, playerNr, clients, udpSocket, multiSocket);
				clients.add(clientThread);
				clientThread.start();
			}
		}catch (Exception e){
			System.out.println(e.toString());
		}
	}
	
}
