import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ClientListener extends Thread {
	
	private DatagramSocket socket;
	private ServerSocket tcpSocket;
	private String serverName;
	
	public ClientListener(DatagramSocket socket, String serverName, ServerSocket tcpSocket){
		this.socket = socket;
		this.tcpSocket = tcpSocket;
		this.serverName =  serverName;
	}
	
	
	
	public void run(){
		while(true){
			try{
				byte[] recieveData = new byte[1024];
				DatagramPacket rececivePacket = new DatagramPacket(recieveData, recieveData.length);
				socket.receive(rececivePacket); //The client waits here until it recieves packets, when that happens that packet is put into recievePacket.
				recieveData = rececivePacket.getData();
				String stringRecieveData = new String(recieveData).trim();
				if(stringRecieveData.equals("JavaGameServer")){
					byte[] sendData = new byte[1024];
					String[] address = tcpSocket.getInetAddress().toString().split("/");
					String stringData = "JavaGameServer," + serverName + "," + address[1] + "," + tcpSocket.getLocalPort();
					sendData = stringData.getBytes(); 
					InetAddress ia = InetAddress.getByName("239.255.255.250");
					DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ia, 1900);
					socket.send(sendPacket);
				}
			} catch (SocketException e1) {
				System.out.println("SocketException" + e1.toString());
			}catch(IOException e2){
				System.out.println("IOException" + e2.toString());
			}
		}
	}

}
