import java.awt.Color;
import java.io.IOException;
import java.net.*;


public class ReceiverThread extends Thread {
	
	private DatagramSocket socket;
	CirclePanel panel;
	
	private int serverPort;
	private int currentPlayer;
	private int circleDiameter;
	private String[] pos2 = new String[4];
	private boolean multicasting;
	
	
	public ReceiverThread(DatagramSocket socket, CirclePanel panel, int serverPort, int currentPlayer, int diameter, boolean multicasting){
		this.socket = socket;
		this.panel = panel;
		this.serverPort = serverPort;
		this.currentPlayer = currentPlayer;
		this.circleDiameter = diameter;
		this.multicasting = multicasting;
		
		
	}
	
	public void run(){
		if(!multicasting){
			try {
				InetAddress serverAddress = InetAddress.getByName("localhost");
				String stringSendData = "Establishing connection \r\n";
				byte[] sendData = new byte[1024];
				sendData = stringSendData.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
				socket.send(sendPacket);
			} catch (SocketException e1) {
				System.out.println("SocketException" + e1.toString());
			}catch(IOException e2){
				System.out.println("IOException" + e2.toString());
			}
		}
		while(true){
			try{
				byte[] recieveData = new byte[1024];
				DatagramPacket rececivePacket = new DatagramPacket(recieveData, recieveData.length);
				socket.receive(rececivePacket); //The client waits here until it recieves packets, when that happens that packet is put into recievePacket.
				recieveData = rececivePacket.getData();
				String stringRecieveData = new String(recieveData);
				pos2 = stringRecieveData.split(",");
				if(!(pos2[0].trim().equals("JavaGameServer")) && pos2[0].trim().matches("^-?\\d+$")){
					if(Integer.parseInt(pos2[0].trim()) == currentPlayer){
						panel.addCircle(new Circle(Integer.parseInt(pos2[1].trim())-(circleDiameter/2), Integer.parseInt(pos2[2].trim())-(circleDiameter/2), circleDiameter, Color.BLUE),currentPlayer); //add playerNr //possible remove old circle before adding new?
					}else{
						panel.addCircle(new Circle(Integer.parseInt(pos2[1].trim())-(circleDiameter/2), Integer.parseInt(pos2[2].trim())-(circleDiameter/2), circleDiameter, Color.RED), Integer.parseInt(pos2[0].trim())); //add other players playerNr pos[3]
					}
					//socket.close();
				}
			} catch (SocketException e1) {
				System.out.println("SocketException" + e1.toString());
			}catch(IOException e2){
				System.out.println("IOException" + e2.toString());
			}
		}	
	}
		
	
}
