import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class InputListener extends MouseAdapter {
	
	private CirclePanel panel;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader input;
	
	byte[] sendData = new byte[1024];
	byte[] recieveData = new byte[1024];
	DatagramSocket clientSocket;
	int udpPort;
	String[] pos2;
	
	private MulticastSocket multiSocket;
	
	private int playerNr;
	private String reply;
	private String[] pos;
	private String[] player;
	private Color ME = Color.BLUE;
	private Color OTHERS = Color.RED;
	private int circleDiameter;
	
	
	public InputListener(CirclePanel panel, Socket s, BufferedReader input, PrintWriter output, int playerNr, DatagramSocket udpSocket, int udpServer, int diameter, MulticastSocket multiSocket) {
		super();
		this.panel = panel;
		this.socket = s;
		this.input = input;
		this.out = output;
		this.circleDiameter = diameter;
		this.pos = new String[3];
		this.pos2 = new String[3];
		
		this.clientSocket = udpSocket;
		this.udpPort = udpServer;
		
		this.multiSocket = multiSocket;
		}

	@Override
	public void mouseClicked(MouseEvent e) {
		//Send requested coordinates to server.
		sendOutput(e); //For tcp
	}
	
	public void sendOutput(MouseEvent e){
		out.println(e.getX() + "," + e.getY());
		out.flush();
	}
	
	public void getPlayerInfo(){
		try{
			reply = input.readLine();
			if(reply.contains("playerNr:")){
				player = reply.split(":");
				playerNr = Integer.parseInt(player[1]);
			}
		}catch(UnknownHostException e1){
			System.out.println("Unkown host exception " + e1.toString());
		}catch (IOException e2){
			System.out.println("IOexception " + e2.toString());
		}catch(IllegalArgumentException e3){
			System.out.println("Illegal argument exception " + e3.toString());
		}catch(Exception e4){
			System.out.println("Other exception " + e4.toString());
		}
		
	}
	
	public void recieveInput(){
		try{
			reply = input.readLine();
			if(reply.contains("playerNr:")){
				player = reply.split(":");
				playerNr = Integer.parseInt(player[1]);
				reply = input.readLine();
			}
			while(reply != null){
				pos = reply.split(",");
				if(Integer.parseInt(pos[0]) == playerNr){
					panel.addCircle(new Circle(Integer.parseInt(pos[1])-(circleDiameter/2), Integer.parseInt(pos[2])-(circleDiameter/2), circleDiameter, ME),playerNr); 
				}else{
					panel.addCircle(new Circle(Integer.parseInt(pos[1])-(circleDiameter/2), Integer.parseInt(pos[2])-(circleDiameter/2), circleDiameter, OTHERS), Integer.parseInt(pos[0])); 
				}
				reply = input.readLine();
			}
			input.close();
			out.close();
			socket.close();
		}catch(UnknownHostException e1){
			System.out.println("Unkown host exception " + e1.toString());
		}catch (IOException e2){
			System.out.println("IOexception " + e2.toString());
		}catch(IllegalArgumentException e3){
			System.out.println("Illegal argument exception " + e3.toString());
		}catch(Exception e4){
			System.out.println("Other exception " + e4.toString());
		}
	}
	
	public void receiveThroughUDP(){
		ReceiverThread udpThread = new ReceiverThread(clientSocket, panel, udpPort, playerNr, circleDiameter, false);
		udpThread.start();

	}
	
	public void receiveThroughMulticast(){
		ReceiverThread udpThread = new ReceiverThread(multiSocket, panel, udpPort, playerNr, circleDiameter, true);
		udpThread.start();

	}
	
}


