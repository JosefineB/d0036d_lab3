


import java.io.IOException;
import java.net.*;
	
public class ConnectionsThread extends Thread {

	private DatagramSocket socket;

	private String[] pos2 = new String[4];
	private ServerBox servers;
	
	
	public ConnectionsThread(DatagramSocket socket, ServerBox servers){
		this.socket = socket;
		this.servers = servers;
		
	}
	
	public void run(){
		byte[] sendData = new byte[1024];
		String stringData = "JavaGameServer";
		sendData = stringData.getBytes(); 
		try {
			InetAddress ia = InetAddress.getByName("239.255.255.250");
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ia, 1900);
			socket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
		}
		while(!(Thread.currentThread().isInterrupted())){
			try{
				byte[] recieveData = new byte[1024];
				DatagramPacket rececivePacket = new DatagramPacket(recieveData, recieveData.length);
				socket.receive(rececivePacket); //The client waits here until it recieves packets, when that happens that packet is put into recievePacket.
				recieveData = rececivePacket.getData();
				String stringRecieveData = new String(recieveData);
				pos2 = stringRecieveData.split(",");
				if(pos2[0].equals("JavaGameServer")){
					servers.addToServerList(pos2[1], InetAddress.getByName(pos2[2]), Integer.parseInt(pos2[3].trim()));
				}
			} catch (SocketException e1) {
				System.out.println("SocketException" + e1.toString());
			}catch(IOException e2){
				System.out.println("IOException" + e2.toString());
			}
		}
	}
}