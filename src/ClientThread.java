import java.net.*;
import java.util.ArrayList;
import java.awt.event.MouseEvent;
import java.io.*;

public class ClientThread extends Thread {
	private Socket socket = null;
	private PrintWriter out;
	private BufferedReader input;
	private PrintWriter broadcasting;
	private ArrayList<ClientThread> clients;
	private InetAddress clientIp;
	private int playerNr;
	private String clientInput;
	private String[] coordinates;
	private int x; 
	private int y;
	
	private DatagramSocket udpSocket;
	private DatagramPacket recievePacket;
	private DatagramPacket sendPacket;
	private int udpPort;
	byte[] sendData = new byte[1024];
	byte[] recieveData = new byte[1024];
	
	private MulticastSocket multiSocket;
	
	public ClientThread(Socket clientSocket, BufferedReader input, PrintWriter out, int playerNr, ArrayList<ClientThread> clients, DatagramSocket udp, MulticastSocket multi){
		this.udpSocket = udp;
		this.socket = clientSocket;
		this.multiSocket = multi;
		this.input = input;
		this.out = out;
		this.clients = clients;
		this.playerNr = playerNr;
		this.coordinates = new String[2];
	}
	
	public void run(){
		try{
			out.println("playerNr:" + playerNr);
			out.flush();
			//recieveUDP(); //For tcp/udp
			while((clientInput = input.readLine()) != null){
				coordinates = clientInput.split(",");
				x = Integer.parseInt(coordinates[0]);
				y = Integer.parseInt(coordinates[1]);
				if(isValidPosition()){
					//broadCast(); //For tcp only
					//broadcastUDP(); //For tcp/udp
					multicast();
				}
			}
			input.close(); //All of these close methods are for specific clients.
			out.close(); //So we need to close them for when each client finishes.
			udpSocket.close();
			multiSocket.close();
			socket.close();
		}catch(Exception e){
			System.out.println(e.toString());
		}
	}

	//Only time this don't work is when one player chooses the exact same position as another player.
	//Otherwise collisions will not be possible. 
	synchronized private boolean isValidPosition(){
		for(ClientThread c: clients){
			double distance = Math.sqrt(((getX() - c.getX())*(getX() - c.getX())) + ((getY() - c.getY())*(getY() - c.getY())));
			if(distance < 24 && distance != 0){ //Diameter of circle
				return false;
			}
		}
		return true;
	}
	
	public int getX(){
		return x-12;
	}
	
	public int getY(){
		return y-12;
	}
	
	
	//First implementation, only tcp used
	public void broadCast(){
		for(ClientThread c : clients){
			//For TCP communication
			broadcasting = c.getOutputStream();
			broadcasting.println(playerNr + "," + x + "," + y);
			broadcasting.flush();
		}
	}
	
	//Second implementation, udp used for updates
	public void broadcastUDP(){
		String stringData = playerNr + "," + x + "," + y;
		sendData = stringData.getBytes(); 
		for(ClientThread c : clients){
			sendPacket = new DatagramPacket(sendData, sendData.length, c.getClientIP(), c.getUDPPort());
			try {
				udpSocket.send(sendPacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	//Last implementation, multicast used to broadcast updates to all clients
	public void multicast(){
		String stringData = playerNr + "," + x + "," + y;
		sendData = stringData.getBytes(); 
		try {
			InetAddress ia = InetAddress.getByName("239.255.255.250");
			sendPacket = new DatagramPacket(sendData, sendData.length, ia, 1900);
			multiSocket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public PrintWriter getOutputStream(){
		return out;
	}
	
	public Socket getSocket(){
		return socket;
	}
	
	public int getPlayerNr(){
		return playerNr;
	}
	
	public InetAddress getClientIP(){
		return clientIp;
	}
	
	public void recieveUDP(){
		recievePacket = new DatagramPacket(recieveData, recieveData.length);
		try {
			udpSocket.receive(recievePacket);
			udpPort = recievePacket.getPort(); //get clients port from header
			clientIp = recievePacket.getAddress(); //extracts the ip address from datagram header.
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	
	public int getUDPPort(){
		return udpPort;
	}
	
}
