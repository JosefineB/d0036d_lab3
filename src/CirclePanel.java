import java.awt.Graphics;
import java.util.LinkedList;

import javax.swing.JPanel;


public class CirclePanel extends JPanel {

	private LinkedList<Circle> circles = new LinkedList<Circle>();
	private int lenght;
	private int size;
	
	public CirclePanel(int size){
		this.size = size;
	}
	
	public void addCircle(Circle circle, int index){
		lenght++;
		try{
			circles.set(index, circle);
		}catch (IndexOutOfBoundsException e){
			int i = circles.size();
			while(i <= index){
				circles.add(i, null);
				i++;
			}
			circles.set(index, circle); //Everytime a click happens a circle is created and addaded to list
		}
		this.repaint(); //This needs to be called to make it so the panel is repainted after each click
	}
	
	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, size, size);
		for(Circle c: circles){ //Each circle in list is paint everytime paint runs.
			if(c != null){
			c.draw(g);
			}
		}
	}
	
	public LinkedList<Circle> getCircles(){
		return circles;
	}
		
	public int getLenght(){
		return lenght;
	}
}
