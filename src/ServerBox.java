import java.net.InetAddress;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ServerBox {
	
	//This implementation can be done to support unlimited servers by using arraylist and turning the lists to arrays
	// using .toArray() for example in the displayList.
	String[] serverNames = new String[10];
	InetAddress[] serverAddress = new InetAddress[10];
	int[] ports = new int[10];
	
	InetAddress selectedServer;
	int selectedPort;
	boolean selected = false;
	int listIndex = 0;
	int size = 0;
	
	
	public void dispalyList() {
		String input = (String) JOptionPane.showInputDialog(null, "Choose server...","Available servers", JOptionPane.QUESTION_MESSAGE, null, serverNames, serverNames[0]); 
		//System.out.println(input);
		selectedServer = getServerAddress(input);
		selectedPort = getServerPort();
		selected = true;
	}
	
	public void addToServerList(String serverName, InetAddress addr, int port){
		serverNames[size] = serverName;
		serverAddress[size] = addr;
		ports[size] = port;
		size++;		
	}

	
	private InetAddress getServerAddress(String serverName){
		for(int i = 0; i<serverNames.length; i++){
			if(serverNames[i].equals(serverName)){
				listIndex = i;
				break;
			}
		}
		return serverAddress[listIndex];
	}
	
	private int getServerPort(){
		return ports[listIndex];
	}
	
	
	public boolean isSelected(){
		return selected;
	}
	
	public InetAddress getAddress(){
		return selectedServer;
	}
	
	public int getPort(){
		return selectedPort;
	}
}


