import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;


public class GameClient extends JFrame {
	
	private final int DIAMETER = 24;
	private final int WINDOWSIZE = 500;

	private Socket socket;
	private int tcpPort;
	private BufferedReader input;
	private PrintWriter output;
	private int playerNr;
	
	private int udpServerPort; //What port the server is listening on
	private DatagramSocket udpSocket; //client socket
	
	private MulticastSocket multiSocket;
	private int multiPort;
	
	public GameClient(int tcpPort, int udpPort, int multiPort){
		//this.tcpPort = tcpPort;
		this.udpServerPort = udpPort;
		this.multiPort = multiPort;
	}
	
	public void initClient(){
		try {
			InetAddress serverAddress = InetAddress.getByName("localhost");
			multiSocket = new MulticastSocket(multiPort);
			InetAddress ia = InetAddress.getByName("239.255.255.250");
			multiSocket.joinGroup(ia);
			ServerBox servers = new ServerBox();
			ConnectionsThread connections = new ConnectionsThread(multiSocket, servers);
			connections.start();
			Thread.sleep(3000);
			connections.interrupt();
			servers.dispalyList();
			boolean showDialog = true;
			while(showDialog){
				if(servers.isSelected()){
					serverAddress = servers.getAddress();
					tcpPort = servers.getPort();
					showDialog = false;
				}
			}
			//Without dynamic discovery
			//serverAddress = InetAddress.getByName("localhost");
			
			socket = new Socket(serverAddress, tcpPort);
			output = new PrintWriter(socket.getOutputStream(), true);
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			//chooses random available port for client for the udp connection
			udpSocket = new DatagramSocket();
		} catch (UnknownHostException e) {
			System.out.println("Server unknown!" + e.toString());
		}catch(IOException e2){
			System.out.println("Server not available!" + e2.toString());
		}catch (InterruptedException e3){
			System.out.println(e3.toString());
		}
		JFrame window = new JFrame("GameClient");
		window.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				try {
					input.close();
					socket.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					System.out.println(e1.toString());
				}
				output.close();
				udpSocket.close();
				multiSocket.close();
			}
		});
		CirclePanel panel = new CirclePanel(WINDOWSIZE);
		window.setContentPane(panel);
		InputListener handler = new InputListener(panel,socket, input, output, playerNr, udpSocket, udpServerPort, DIAMETER, multiSocket);
		panel.addMouseListener(handler);
		window.setSize(WINDOWSIZE, WINDOWSIZE);
		window.setVisible(true);
		window.setResizable(true);
		initIpv6LookUp(window);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//handler.recieveInput(); //When using tcp only
		handler.getPlayerInfo();
		//handler.receiveThroughUDP(); //when using tcp and udp
		handler.receiveThroughMulticast();
	}
		
	private void initIpv6LookUp(JFrame window){
		JDialog dialog = new JDialog(window,"Ipv6"); 
	    JPanel pan = new JPanel();
	    JLabel text = new JLabel("Enter address: ");
	    JLabel text2 = new JLabel("IP address: ");
	    JLabel output = new JLabel();
	    JTextField textfield = new JTextField(8);
	    textfield.setBounds(10, 10, 40, 20);
	    JButton button = new JButton("OK");
	    button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String input = textfield.getText();
				try {
					InetAddress[] addresses = InetAddress.getAllByName(input);
					Inet6Address address = getIPv6Addresses(addresses);
					if(address != null){
						output.setText(address.toString());
					}
					else{
						output.setText(addresses[0].toString());
					}
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					output.setText("Invalid address!");
				}
			}
	    	
	    });
	    pan.add(text);
	    pan.add(textfield);
	    pan.add(button);
	    pan.add(text2);
	    pan.add(output);
	    pan.setSize(200,200);
	    dialog.add(pan);
	    dialog.setSize(200, 200);
	    dialog.setLocationRelativeTo(null);
		dialog.setModalityType(null);
		dialog.setVisible(true);
	}
	
	private Inet6Address getIPv6Addresses(InetAddress[] addresses) {
	    for (InetAddress addr : addresses) {
	    	System.out.println(addr.toString());
	        if (addr instanceof Inet6Address) {
	            return (Inet6Address) addr;
	        }
	    }
	    return null;
	}
	
	
	
	public void paint(Graphics g){
		
	}
		

}
